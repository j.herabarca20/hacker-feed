const express = require('express');
const ArticlesController = require('../controllers/articles.controller');
const api = express.Router();

api.get('/articles', ArticlesController.getArticles);
api.put('/article/:id', ArticlesController.deleteArticle);
api.post('/article', ArticlesController.fillArticles);


module.exports = api;
