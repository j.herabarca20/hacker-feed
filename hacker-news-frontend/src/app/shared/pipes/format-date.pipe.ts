import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

  now: any = moment().format('DD/MM/YYYY HH:mm:ss');
  midnight: any = moment().startOf('day');
  midnightBefore: any = moment().subtract(1, 'day');

  transform(create_ad: Date): any {

    if (create_ad) {

      let seconds = Math.floor((+new Date() - +new Date(create_ad)) / 1000);
      let timeToday = moment(this.now, 'DD/MM/YYYY HH:mm:ss').diff(moment(this.midnight, 'DD/MM/YYYY HH:mm:ss'));
      let timebefore = moment(this.now, 'DD/MM/YYYY HH:mm:ss').diff(moment(this.midnightBefore, 'DD/MM/YYYY HH:mm:ss'));
      let today = moment(timeToday).unix();
      let antesdeayer = moment(timebefore).unix();
      let beforeYesterday = antesdeayer + today;

      if (today > seconds) {
        return moment(create_ad).format('HH:mm a');
      }
      if (seconds < beforeYesterday) {
        return moment(create_ad).subtract('days').calendar({ lastDay: '[Yesterday]' });
      }
      if (seconds > beforeYesterday) {
        return moment(create_ad).format('MMM DD');
      }
    }
    
    return create_ad;
  }

}
