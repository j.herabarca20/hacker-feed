# Hacker news  feed

This project  is published as part of a test for reign company.

## Before using in your environment local

- Please make sure that you have:
 - node.js installed (https://nodejs.org/)
 - have mongodb installed and running locally (https://www.mongodb.com/)
 - have angular in your last version.
 - have docker in your last version.

## Usage

To run the project, please use a command line the following:

- docker-compose build
- docker-compose up

For view ower project runing  is nesecary go to link http://localhost:4200/

the information will be loaded  al open the url above indicated

if you want to review the created apis, click on the following url(Postman Collection). https://www.getpostman.com/collections/51dc3d3b7b4bc125677f

